﻿// Created by Juan Sebastian Munoz
// naruse@gmail.com
// pencilsquaregames.com

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public Color hintColor = Color.white;
    
    public GameObject[] sampleGems;
    public float timeToHelpUser = 3f;

    private float timeToDisplayHint;
    private bool showHints = true;
    public int boardWidth = 7;
    public int boardHeight = 8;
    private GameObject[,] board;

    private float gemSize = 0.5f;
    
    //contains the list with adjacent objects a gem has when user clicks
    private List<GameObject> userSelectedGems;
    private List<List<GameObject>> possibleMoves;
    private List<GameObject> hintedGems;

    private int score = 0;
    public int Score { get { return score; } }

    private const float timeToStillHaveMultiplier = 3;
    private float timeToResetScoreMultiplier = 0;
    private const int maxScoreMultiplier = 3;

    private int scoreMultiplier = 1;
    public int Multiplier { get { return scoreMultiplier; } }

    public class Pos {
        public int i { get; set; }
        public int j { get; set; }
        public Pos(int _i, int _j) {
            i = _i;
            j = _j;
        }
    }

	void Start () {
        timeToDisplayHint = timeToHelpUser;
        userSelectedGems = new List<GameObject>();
        possibleMoves = new List<List<GameObject>>();
        hintedGems = new List<GameObject>();

        GenerateGame();
	}

    private void GenerateGame() {
        board = new GameObject[boardWidth, boardHeight];
        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                board[i,j] = GenerateGemAtPos(i, j);
            }
        }
    }

    private GameObject GenerateGemAtPos(int i, int j, int gemType = -99) {
        int generatedGemType = gemType;
        if(generatedGemType == -99)
            generatedGemType = Random.Range(0,sampleGems.Length);

        GameObject selectedGem = sampleGems[generatedGemType];
        GameObject generatedGem = Instantiate(selectedGem, GetGemPos(i, j), selectedGem.transform.rotation) as GameObject;
        generatedGem.AddComponent<GemLogic>();
        generatedGem.GetComponent<GemLogic>().SetGemPos(i, j);
        generatedGem.GetComponent<GemLogic>().SetTypeAndReference(generatedGemType, this);
        generatedGem.GetComponent<GemLogic>().SetColor(sampleGems[generatedGemType].renderer.material.color);
        generatedGem.SetActive(true);
        generatedGem.name = "Gem";
        return generatedGem;
    }

    private Vector3 GetGemPos(int i, int j) {
        float posX = i*gemSize -((float)boardWidth)/4 + ((boardWidth%2 == 0) ? gemSize : gemSize/2);
        float posZ = j*gemSize -((float)boardHeight)/4 + ((boardHeight%2 == 0) ? gemSize : gemSize/2);
        float posY = gemSize/2;

        return new Vector3(posX, posZ, posY);
    }

	void Update () {
        ApplyGameLogic();

        if(Input.GetMouseButtonUp(0)) {
            ClearHintedGems();
            DeselectAllGems();
            showHints = true;
            timeToDisplayHint = Time.time + timeToHelpUser;
            GenerateMoveIfNecessary();
        }

        if(Time.time > timeToResetScoreMultiplier)
            scoreMultiplier = 1;

        if(Time.time > timeToDisplayHint && showHints) {
            HintGems();
            Debug.Log("HINT GEMS");
            showHints = false;
        }
	}

    private void ApplyGameLogic() {
        if(userSelectedGems.Count > 2) {
            score = score + (userSelectedGems.Count * scoreMultiplier);
            timeToResetScoreMultiplier = Time.time + timeToStillHaveMultiplier;//increase time to still be able to keep the multiplier alive.
            scoreMultiplier = scoreMultiplier + (scoreMultiplier < maxScoreMultiplier ? 1 : 0);
            for(int i = 0; i < userSelectedGems.Count; i++) {
                Destroy(userSelectedGems[i]);
                ExplosionManager.Instance.CreateExplosion(userSelectedGems[i].transform.position, userSelectedGems[i].GetComponent<GemLogic>().GemColor);
            }
        }
        userSelectedGems.Clear();

        while(NeedToFillBoard()) {
            GenerateTopGems();
            ApplyGravity();
        }
    }

    private void HintGems() {
        ClearHintedGems();
        GenerateMoveIfNecessary();
        //find the largest match!
        int largestAdjacentBlocks = -1;
        int optimalIndex = 0;
        for(int i = 0; i < possibleMoves.Count; i++)
            if(largestAdjacentBlocks < possibleMoves[i].Count) {
                largestAdjacentBlocks = possibleMoves[i].Count;
                optimalIndex = i;
            }
        for(int i = 0; i < possibleMoves[optimalIndex].Count; i++) {
            hintedGems.Add(possibleMoves[optimalIndex][i]);//we add this into a list in case we want to do something with the hinted gems
            hintedGems[hintedGems.Count-1].renderer.material.color = hintColor;
        }


    }

    private void ClearHintedGems() {
        for(int i = 0; i < hintedGems.Count; i++) {
            Destroy(hintedGems[i]);
        }
        hintedGems.Clear();
    }

    private void GenerateMoveIfNecessary() {
        possibleMoves = GetAvailableMoves();
        if(possibleMoves.Count == 0) {
            //no more moves. lets generate a move.
            Debug.Log("No more moves, generating one...");
            GenerateMove();
            possibleMoves = GetAvailableMoves();
        }
    }

    //Modifies the board a lil bit and creates a move.
    private void GenerateMove() {
        int i = Random.Range(0, boardWidth);
        int j = Random.Range(0, boardHeight);

        Pos[] adjacentPos = GenerateTwoValidAdjacentPos(i,j);

        Destroy(board[adjacentPos[0].i, adjacentPos[0].j]);
        Destroy(board[adjacentPos[1].i, adjacentPos[1].j]);

        board[adjacentPos[0].i, adjacentPos[0].j] = GenerateGemAtPos(adjacentPos[0].i, adjacentPos[0].j, board[i,j].GetComponent<GemLogic>().GemType);
        board[adjacentPos[1].i, adjacentPos[1].j] = GenerateGemAtPos(adjacentPos[1].i, adjacentPos[1].j, board[i,j].GetComponent<GemLogic>().GemType);
    }

    //generates a valid tuple from i,j within the limits of the board
    //the generated positions are different from i,j and dif from each other.
    private Pos[] GenerateTwoValidAdjacentPos(int i, int j) {
        Pos[] generatedPositions = new Pos[2];

        Pos[] posibleCombinations = { new Pos(i+1,j+0), new Pos(i+0,j+1), new Pos(i-1,j+0), new Pos(i+0,j-1) };

        int index1 = Random.Range(0,1);
        int index2 = Random.Range(0,1);

        while(!ValidPosition(posibleCombinations[index1])) {
            index1 = (index1+1) % posibleCombinations.Length;
        }
        while(index1 == index2 || !ValidPosition(posibleCombinations[index2])) {
            index2 = (index2+1) % posibleCombinations.Length;
        }
        generatedPositions[0] = posibleCombinations[index1];
        generatedPositions[1] = posibleCombinations[index2];


        return generatedPositions;
    }

    private bool ValidPosition(Pos p) {
        return p.i >= 0 && p.i < boardWidth && p.j >= 0 && p.j < boardHeight;
    }

    private List<List<GameObject>> GetAvailableMoves() {
        List<List<GameObject>> availableMoves = new List<List<GameObject>>();
        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                SelectAdjacentGems(i, j, board[i,j].GetComponent<GemLogic>().GemType, false/*dont paint selected gems*/);
                if(userSelectedGems.Count > 2) {
                    availableMoves.Add(new List<GameObject>(userSelectedGems));
                }
                for(int k = 0; k < userSelectedGems.Count; k++)
                    userSelectedGems[k].GetComponent<GemLogic>().UnSelectGem();
                userSelectedGems.Clear();
            }
        }
        return availableMoves;
    }

    private bool NeedToFillBoard() {
        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                if(board[i,j] == null)
                    return true;
            }
        }
        return false;
    }

    private void ApplyGravity() {
        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                if(j-1 >= 0) {
                    if(board[i,j-1] == null && board[i,j] != null) {
                        board[i,j-1] = board[i,j];
                        board[i,j-1].transform.position = GetGemPos(i,j-1);
                        board[i,j-1].GetComponent<GemLogic>().SetGemPos(i,j-1);
                        board[i,j] = null;
                    }
                }
            }
        }
    }

    private void GenerateTopGems() {
        for(int i = 0; i < boardWidth; i++) {
            if(board[i, boardHeight-1] == null)
                board[i, boardHeight-1] = GenerateGemAtPos(i, boardHeight-1);
        }
    }

    private void PrintMatrix() {
        string s = "";
        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                s += (board[i,j] != null ? " X " : " n ");
            }
            s += "\n";
        }
        Debug.Log(s);
    }



    public void SelectAdjacentGems(int i, int j, int gemType, bool paintSelected = true) {
        board[i,j].GetComponent<GemLogic>().SelectGem(paintSelected);

        userSelectedGems.Add(board[i,j]);

        if(i+1 < boardWidth)
            if(board[i+1,j] != null && board[i+1,j].GetComponent<GemLogic>().GemType == gemType &&
               !board[i+1,j].GetComponent<GemLogic>().GemIsSelected)
                SelectAdjacentGems(i+1, j, gemType, paintSelected);
        if(i-1 >= 0)
            if(board[i-1,j] != null && board[i-1,j].GetComponent<GemLogic>().GemType == gemType &&
               !board[i-1,j].GetComponent<GemLogic>().GemIsSelected)
                SelectAdjacentGems(i-1, j, gemType, paintSelected);


        if(j+1 < boardHeight)
            if(board[i,j+1] != null && board[i,j+1].GetComponent<GemLogic>().GemType == gemType &&
               !board[i,j+1].GetComponent<GemLogic>().GemIsSelected)
                SelectAdjacentGems(i, j+1, gemType, paintSelected);
        if(j-1 >= 0)
            if(board[i,j-1] != null && board[i,j-1].GetComponent<GemLogic>().GemType == gemType &&
               !board[i,j-1].GetComponent<GemLogic>().GemIsSelected)
                SelectAdjacentGems(i, j-1, gemType, paintSelected);
    }

    private void DeselectAllGems() {
        for(int i = 0; i < boardWidth; i++) {
            for(int j = 0; j < boardHeight; j++) {
                if(board[i,j] != null)
                    board[i,j].GetComponent<GemLogic>().UnSelectGem();
            }
        }
    }

    public void CheckForResetMultiplier() {
        if(userSelectedGems.Count <= 2) {
            scoreMultiplier = 1;
        }
    }
}

